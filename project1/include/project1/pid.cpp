#include <project1/pid.h>
#include <stdio.h>

PID::PID(){

    /* TO DO
     *
     * find appropriate value of the coefficients for PID control, and initialize them.
     *
    */

     Kp = 0.5;
     Ki = 0.05;
     Kd = -0.05;
     error = 0.0;
     error_sum = 0.0;
     error_diff = 0.0;
}

float PID::get_control(point car_pose, point goal_pose){

    float ctrl;

    /* TO DO
     *
     * implement pid algorithm
     *
    */
	
	float cur_err;
	float goal;
	
	goal= atan2((goal_pose.y - car_pose.y) , (goal_pose.x - car_pose.x));
	
	printf("goal is %.2f\n", goal);
	cur_err = goal - car_pose.th;
	ctrl = Kp * cur_err + Ki * 0.1 * error_sum + Kd * 10 * (cur_err - error);
	error_sum += cur_err;
	error = cur_err;

    return ctrl;
}
