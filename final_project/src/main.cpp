//state definition
#define INIT 0
#define PATH_PLANNING 1
#define RUNNING 2
#define FINISH -1

#include <unistd.h>
#include <ros/ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Time.h>
#include <ackermann_msgs/AckermannDriveStamped.h>
#include <project2/rrtTree.h>
#include <tf/transform_datatypes.h>
#include <project2/pid.h>
#include <math.h>
#include <pwd.h>

#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include <ctime>
#include <cstdlib>

//map spec
cv::Mat map;
double res;
int map_y_range;
int map_x_range;
double map_origin_x;
double map_origin_y;
double world_x_min;
double world_x_max;
double world_y_min;
double world_y_max;

//parameters we should adjust : K, margin, MaxStep
int margin = 5;
int K = 2000;
double MaxStep = 2.0;
int waypoint_margin = 10;

PID pid_ctrl;

//way points
std::vector<point> waypoints;

//path
std::vector<traj> path_RRT;

//robot
point robot_pose;
ackermann_msgs::AckermannDriveStamped cmd;

//FSM state
int state;

//function definition
void setcmdvel(double v, double w);
void callback_state(geometry_msgs::PoseWithCovarianceStampedConstPtr msgs);
void set_waypoints();
void generate_path_RRT();

int main(int argc, char** argv){
    ros::init(argc, argv, "slam_main");
    ros::NodeHandle n;

    // Initialize topics
    ros::Publisher cmd_vel_pub = n.advertise<ackermann_msgs::AckermannDriveStamped>("/vesc/high_level/ackermann_cmd_mux/input/nav_0",1);
    
    ros::Subscriber gazebo_pose_sub = n.subscribe("/amcl_pose", 100, callback_state);

    printf("Initialize topics\n");
    
    // FSM
    state = INIT;
    bool running = true;
    int look_ahead_idx;
    ros::Rate control_rate(60);

    while(running){
        switch (state) {
        case INIT: {
            look_ahead_idx = 0;
            // Load Map
            char* user = getpwuid(getuid())->pw_name;
            cv::Mat map_org = cv::imread((std::string("/home/") + std::string(user) +
                              std::string("/catkin_ws/src/final_project/src/final.pgm")).c_str(), CV_LOAD_IMAGE_GRAYSCALE);

            cv::transpose(map_org,map);
            cv::flip(map,map,1);

            map_y_range = map.cols;
            map_x_range = map.rows;
            map_origin_x = map_x_range/2.0 - 0.5;
            map_origin_y = map_y_range/2.0 - 0.5;
            world_x_min = -4.7;
            world_x_max = 4.7;
            world_y_min = -10.2;
            world_y_max = 10.2;
            res = 0.05;
            printf("Load map\n");

             if(! map.data )                              // Check for invalid input
            {
                printf("Could not open or find the image\n");
                return -1;
            }
            state = PATH_PLANNING;
        } break;

        case PATH_PLANNING:
            
            // Set Way Points
            set_waypoints();
            printf("Set way points\n");

            // RRT
            generate_path_RRT();
            printf("Generate RRT\n");

            ros::spinOnce();
            ros::Rate(0.33).sleep();
            
            printf("Initialize ROBOT\n");
            state = RUNNING;

        case RUNNING: {
            //TODO3
            ros::spinOnce();
	        control_rate.sleep();
            float ctrl = pid_ctrl.get_control(robot_pose, path_RRT[look_ahead_idx], path_RRT[look_ahead_idx+1]);
            setcmdvel(1.2, ctrl); // Should be Modified!!
            if (fabs(ctrl) > 0.2) setcmdvel(1.0, ctrl);
            cmd_vel_pub.publish(cmd);
            float distance_left = sqrt(pow((robot_pose.x - path_RRT[look_ahead_idx+1].x), 2) + pow((robot_pose.y - path_RRT[look_ahead_idx+1].y), 2));
            if (distance_left < 0.5) // Should be Modified!!
            {
                
                look_ahead_idx++;
                pid_ctrl.reset();
            }

            if (look_ahead_idx == path_RRT.size()-1)
                state = FINISH;
        } break;

        case FINISH: {
            setcmdvel(0,0);
            cmd_vel_pub.publish(cmd);
            running = false;
            ros::spinOnce();
            control_rate.sleep();
        } break;
        default: {
        } break;
        }
    }
    return 0;
}

void setcmdvel(double vel, double deg){
    cmd.drive.speed = vel;
    cmd.drive.steering_angle = deg;
}

void callback_state(geometry_msgs::PoseWithCovarianceStampedConstPtr msgs){
    robot_pose.x = msgs->pose.pose.position.x;
    robot_pose.y = msgs->pose.pose.position.y;
    robot_pose.th = tf::getYaw(msgs->pose.pose.orientation);
    //printf("x,y : %f,%f \n",robot_pose.x,robot_pose.y);
}

void set_waypoints()
{
    point waypoint_candid[7];

    // Starting point. (Fixed)
    waypoint_candid[0].x = -3.5;
    waypoint_candid[0].y = 8.5;


    //TODO 2
    // Set your own waypoints.
    // The car should turn around the outer track once, and come back to the starting point.
    // This is an example.
    // waypoint_candid[1].x = 2.2;
    // waypoint_candid[1].y = 8.5;
    // waypoint_candid[2].x = 2.5;
    // waypoint_candid[2].y = -8.5;
    // waypoint_candid[3].x = -2.5;
    // waypoint_candid[3].y = -8.0;
    // waypoint_candid[4].x = -3.5;
    // waypoint_candid[4].y = 8.5;
    
    std::srand(std::time(NULL));
    
    cv::Mat map_margin = map.clone();
    int jSize = map.cols; // the number of columns
    int iSize = map.rows; // the number of rows

    for (int i = 0; i < iSize; i++) {
        for (int j = 0; j < jSize; j++) {
            if (map.at<uchar>(i, j) < 125) {
                for (int k = i - waypoint_margin; k <= i + waypoint_margin; k++) {
                    for (int l = j - waypoint_margin; l <= j + waypoint_margin; l++) {
                        if (k >= 0 && l >= 0 && k < iSize && l < jSize) {
                            map_margin.at<uchar>(k, l) = 0;
                        }
                    }
                }
            }
        }
    }
    int mid_col = jSize/2;
	int mid_row = iSize/2;
	int i_idx_quad = waypoint_candid[0].x/res + map_origin_x - mid_row;
	int j_idx_quad = waypoint_candid[0].y/res + map_origin_y - mid_col;
	float rand_x = i_idx_quad;
	float rand_y = j_idx_quad;
    
	for (int i = 1; i < 4; i++){

		int coeff_i = 1;
		int coeff_j = 1;
		if (rand_x >= 0 && rand_y >= 0){
			coeff_i = 1;
			coeff_j = -1;
		}
		else if (rand_x >= 0 && rand_y < 0){
			coeff_i = -1;
			coeff_j = -1;
		}
		else if (rand_x < 0 && rand_y >= 0){
			coeff_i = 1;
			coeff_j = 1;
		}
		else if(rand_x < 0 && rand_y < 0){
			coeff_i = -1;
			coeff_j = 1;
		}

		do{
			// rand_x = (std::rand() % mid_row) * coeff_i;
			// rand_y = (std::rand() % mid_col) * coeff_j;
            
            rand_x = ((std::rand() % 10) / 100.0) * mid_row;
            rand_y = ((std::rand() % 10) / 100.0) * mid_col;
            rand_x += 0.75 * mid_row;
            rand_y += 0.75 * mid_col;
            rand_x *= coeff_i;
            rand_y *= coeff_j;

		} while(map_margin.at<uchar>(int(rand_x)+mid_row, int(rand_y)+mid_col) <= 128);

		waypoint_candid[i].x = res*(rand_x + mid_row - map_origin_x);
		waypoint_candid[i].y = res*(rand_y + mid_col - map_origin_y);
        
		std::cout <<" x = " << waypoint_candid[i].x << "\ty= " << waypoint_candid[i].y << std::endl;
	}
    waypoint_candid[4].x = -3.5;
    waypoint_candid[4].y = 8.5;

    // Waypoints for arbitrary goal points.
    // TA will change this part before scoring.
    // This is an example.
    waypoint_candid[5].x = 1.5;
    waypoint_candid[5].y = 1.5;
    waypoint_candid[6].x = -2;
    waypoint_candid[6].y = -9.0;

    int order[] = {0,1,2,3,4,5,6};
    int order_size = 7;

    for(int i = 0; i < order_size; i++){
        waypoints.push_back(waypoint_candid[order[i]]);
    }
}

void generate_path_RRT()
{   
    //TODO 1

    int iteration = 5;
    int treeNum = waypoints.size() - 1;
    rrtTree* tree = new rrtTree[treeNum];
	std::vector<traj> temp_min_path;
	for (int i = 0; i < treeNum; i++)
	{
		int min_len = 999;
		std::cout << "-------------path#" << i+1 << " is generating-------------" << std::endl;
		for (int l = 0; l < iteration; l++)
		{
			std::vector<traj> temp_path;
			std::cout << "  iteration #" << l+1 << "\t[";
			for (int k = 0; k <= l; k++)
				std::cout << " * ";
			for (int k = l; k < iteration-1; k++)
				std::cout << " - ";
			std::cout << "]" << std::endl;
        	tree[i] = rrtTree(waypoints[i], waypoints[i+1] ,map ,map_origin_x, map_origin_y, res, margin);
	        tree[i].generateRRT(world_x_max, world_x_min, world_y_max, world_y_min, K, MaxStep);
    	    temp_path = tree[i].backtracking_traj();

			if(temp_path.size() < min_len)
			{
				temp_min_path.clear();
				min_len = temp_path.size();
	        	for (int j = temp_path.size() - 1; j >= 0; j--)
				{
					temp_min_path.push_back(temp_path[j]);
				}
			}
               std::cout << "path size " << temp_path.size() << std::endl;
    	}
       	for (int j = 0; j < temp_min_path.size(); j++)
		{
           	path_RRT.push_back(temp_min_path[j]);
		}
	}


}
